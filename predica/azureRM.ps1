Param($groupName, $resourceTypes);

$groupName = "TEST";
$resourceTypes = "postgres db";

if (!$groupName) {
    Write-Host Group name is not defined.
    #Pause
    #Close
}

if (!$resourceTypes) {
    Write-Host Resource types are not defined.
    #Pause
    #Close
}

az login 
Pause;

Write-Host $groupName;

if (az group exists -n $groupName) {

    #remove default Types
    RemoveResourceByType("webapp");
    RemoveResourceByType("sql");
    #remove custom
    foreach ($drt in $resourceTypes) {           
        RemoveResourceByType($drt);        
    }   

}
else {
    Write-Host Group + $groupName + does not exists;
}

function RemoveResourceByType {
    param ($resourceTypeName);


    $cmd = "az " + $resourceTypeName + " list --resource-group " + $groupName;
    $resources = &$cmd;

    if (!$?) {
        Write-Host $_.Exception.Message
        $continue = Read-Host "Do you want to continue? (y/n)";
    
        if (!$continue) {
            Close;
        }
    }
    else {

        foreach ($resource in $resources) {
            Write-Host You are going to remove $resource
            Pause;
        }
    }
}


az logout;
pause;