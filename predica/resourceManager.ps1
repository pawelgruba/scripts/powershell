Param($groupName, $resourceTypes);

#$groupName = "TEST";
#$resourceTypes = "postgres db";


function CheckParameters() {
    if (!$groupName) {
        Write-Host Group name is not defined.
        Pause
        Exit;
    }
    
    if (!$resourceTypes) {
        Write-Host Resource types are not defined.
        Pause
        Exit
    }
}

function CheckAndInstallModule() {
    if (!(Get-Module -ListAvailable -Name AzureRm)) { 
        try {
            $installModule = Read-Host "Module does not exist, do you want to install it? (y/n)"

            if ($installModule -eq "y") {
                Install-Module AzureRM -ErrorAction Stop
            }
            else {
                Exit;
            }        
        }
        catch {
            throw $_.Exception            
        }
    }
}

function CheckGroupExist() {
    try {
        Get-AzureRmResourceGroup -Name $groupName -ErrorVariable notPresent -ErrorAction SilentlyContinue

        if ($notPresent) {
            Write-Host $groupName "Provided group name does not exists";
            Exit;
        }    
    }
    catch {
        throw $_.Exception            
    }
}

function RemoveResourceByType {
    param ($typeName);

    try {
        $cmd = "Get-$($typeName) -ResourceGroupName $($groupName)";
        #Write-Host $cmd;
        $resources = Invoke-Expression $cmd;

        foreach ($resource in $resources) {
            $cmd = "Remove-$($typeName) -ResourceGroupName $($groupName) -Name $($resource.name)";
            #Write-Host $cmd;
            Invoke-Expression $cmd;
        }
    }
    catch {
        throw $_.Exception            
    }    
}


try {
    CheckParameters;
    CheckAndInstallModule;
    Connect-AzureRmAccount
    CheckGroupExist;
    RemoveResourceByType("AzureRmWebApp");
    RemoveResourceByType("AzureRmSqlServer");

    foreach($additionalType in $resourceTypes){
        RemoveResourceByType($additionalType);
    }
    
    Disconnect-AzureRmAccount;
}
catch {
    Write-Host $_.Exception.Message
    Pause;
    Disconnect-AzureRmAccount;

}
