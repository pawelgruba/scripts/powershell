﻿$targetPath = 'D:\Dokumenty\Obrazy\zdjecia_nasze\tel_pawel'
$sourcePath = 'D:\Dokumenty\Obrazy\zdjecia_nasze\z_aparatu_do_podzialu'
$existingFiles = Get-Content $targetPath\log.txt

#robocopy $sourcePath\DCIM\102_PANA\ $targetPath /XC /XN /XO
foreach ($item in Get-ChildItem $sourcePath -Recurse -Exclude $existingFiles )
{
    move-item $item.FullName -Destination $targetPath 
    #$item.Name >> $targetPath\log.txt -Encoding "UTF8"
    Write-Output $item.Name  | Out-File -FilePath $targetPath\log.txt  -Encoding utf8 -Append
    Write-Output Skopiowano $item.FullName
}
PAUSE