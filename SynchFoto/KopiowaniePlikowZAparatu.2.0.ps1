﻿$targetImagePath = 'D:\Dokumenty\Obrazy\zdjecia_nasze\SynchZAparatu'
$targetMoviePath = 'D:\Dokumenty\Wideo\SynchZAparatu'
$sourcePath = ""
#declaring empty array:
$existingImageFiles = @()
$existingMoviesFiles = @()


#determine do you want to copy movies or photos

function CopyImages  
{
    foreach ($item in Get-ChildItem $sourcePath -Recurse -Include *.JPG,*.jpg -Exclude $existingImageFiles )
    {
        copy-item $item.FullName -Destination $targetImagePath 
        Write-Output $item.Name  | Out-File -FilePath $targetImagePath\log.txt  -Encoding utf8 -Append
        Write-Output Skopiowano $item.FullName
    }
}

function CopyMovies
{
    foreach ($item in Get-ChildItem $sourcePath -Recurse -Include *.mp4,*.MTS -Exclude $existingMoviesFiles )
    {
        copy-item $item.FullName -Destination $targetMoviePath 
        Write-Output $item.Name  | Out-File -FilePath $targetMoviePath\log.txt  -Encoding utf8 -Append
        Write-Output Skopiowano $item.FullName
    }
}

function MainMenu
{
    Write-Output "1: Images (.jpg)"
    Write-Output "2: Movies (.mp4)"
    Write-Output "0: Close"
    $choosedOption = Read-Host "What kind of data do you want to synchronize?"

    switch ($choosedOption) {
        1 { CopyImages}
        2 { CopyMovies}
        3 { Close }
        Default 
            {
                Write-Output "Incorrect, try again"
                MainMenu
            }
    }
}

function CheckLogExists
{
    if (Test-Path $targetImagePath\log.txt)
    {
        $script:existingImageFiles = Get-Content $targetImagePath\log.txt    
    }

    if (Test-Path $targetMoviePath\log.txt)
    {
        $script:existingMoviesFiles = Get-Content $targetMoviePath\log.txt    
    }
}


#Właściwe wywołanie programu:

$sourcePath = Read-Host "Enter drive letter to synch"
$sourcePath = $sourcePath + ':'


CheckLogExists

MainMenu
