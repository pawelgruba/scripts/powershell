﻿Function Get-Folder($initialDirectory) {
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null

    $foldername = New-Object System.Windows.Forms.FolderBrowserDialog
    $foldername.Description = "Select a folder"
    $foldername.rootfolder = "MyComputer"

    if ($foldername.ShowDialog() -eq "OK") {
        $folder += $foldername.SelectedPath
    }
    return $folder
}

Function Main() {
    New-Item -ItemType directory -Path $newLocation
    Set-Location -Path $newLocation

    Write-Host ====================================================
    Write-Host Creating Solution file
    dotnet new sln

    Write-Host ====================================================
    Write-Host Creating Application class project in Application layer
    $projName = $solutionName + ".Application";
    dotnet new classlib -f netcoreapp2.2 -n $projName -o (".\Business\" + $projName)

    Write-Host ====================================================
    Write-Host Creating Domain class project in Application layer
    $projName = $solutionName + ".Domain";
    dotnet new classlib -f netcoreapp2.2 -n $projName -o (".\Business\" + $projName)

    Write-Host ====================================================
    Write-Host Creating Data class project in Data layer
    $projName = $solutionName + ".Data";
    dotnet new classlib -f netcoreapp2.2 -n $projName -o (".\Data\" + $projName)


    $projName = "Web";
    Write-Host ====================================================
    Write-Host Creating Web anbular project in Interface layer
    dotnet new angular -n $projName -o (".\Interface\" + $projName);

    Write-Host ====================================================
    Write-Host Adding projects to solution...
    dotnet sln add (".\Interface\" + $projName)
    dotnet sln add (".\Business\" + $solutionName + ".Application")
    dotnet sln add (".\Business\" + $solutionName + ".Domain")
    dotnet sln add (".\Data\" + $solutionName + ".Data")

    Set-Location ".\Interface\Web"

    Remove-Item ClientApp -Force -Recurse
    npm install -g @angular/cli@latest
    ng new ClientApp

    Write-Host "....I'm done";
    exit;
}

##############################################
#Begin of the script:
Clear-Host;
$a = Get-Folder

Write-Host ====================================================
$solutionName = Read-Host 'Enter name for new solution';
Write-Host $solutionName;

Set-Location -Path $a
$newLocation = $a + "\" + $solutionName;
if (!(Test-Path -Path $newLocation )) {
    Main;
}
else {
    $shouldRemove = Read-Host "Directory already exists, do you want to remove it? (y/n)";
    if ($shouldRemove -eq "y") {
        Remove-Item -Path $newLocation -Force -Recurse -Verbose;
        Main;
    }
    else {
        Write-Host This window will be closed;
        Pause;
        exit
    }
}



